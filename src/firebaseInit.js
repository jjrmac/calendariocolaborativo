import { FirebaseApp, initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getDatabase } from "firebase/database";

var firebaseConfig = {
  apiKey: "AIzaSyAK3PvS0cbXB7Ze-15icD79lHKQYFGNRFg",
  authDomain: "calendario-colaborativo-8e63c.firebaseapp.com",
  projectId: "calendario-colaborativo-8e63c",
  storageBucket: "calendario-colaborativo-8e63c.appspot.com",
  messagingSenderId: "287050532935",
  appId: "1:287050532935:web:69fa90a3298c0c09450962"
};

// Initialize Firebase
export default firebase.initializeApp(firebaseConfig);