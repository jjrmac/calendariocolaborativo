import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import firebase from 'firebase/compat/app'
import 'firebase/compat/firestore'

import VueTextareaAutosize from 'vue-textarea-autosize'

Vue.use(VueTextareaAutosize)

Vue.config.productionTip = false

firebase.initializeApp({
  apiKey: "AIzaSyAK3PvS0cbXB7Ze-15icD79lHKQYFGNRFg",
  authDomain: "calendario-colaborativo-8e63c.firebaseapp.com",
  databaseURL: "https://calendario-colaborativo-8e63c-default-rtdb.firebaseio.com",
  projectId: "calendario-colaborativo-8e63c",
  storageBucket: "calendario-colaborativo-8e63c.appspot.com",
  messagingSenderId: "287050532935",
  appId: "1:287050532935:web:69fa90a3298c0c09450962"
});

export const db = firebase.firestore();


new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
